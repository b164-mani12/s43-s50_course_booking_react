const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth")

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		username: reqBody.username,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//Saves the created object to our database
	return newUser.save().then((user, error) => {
		//User registration failed
		if(error) {
			return false;
		} else {
			//User Registration is success
			return true
		}
	})

}