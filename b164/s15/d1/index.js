console.log("Hello World")

let assignNumb = 8;
assignNumb = assignNumb + 2;
console.log(assignNumb);

assignNumb += 2;
console.log(assignNumb);

//increment
//pre-fix incrementation.
let z = 1;
++z;
console.log(z);

//post-fix incrementation
z++;
console.log(z);
console.log(z++);
console.log(z);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE);
console.log(typeof numE);

//comparison operators
let juan = "juan";

// (==) equality operator

console.log(1==1); //true
console.log(1==2); //false
console.log(1=='1'); //true
console.log(0==false) //true
console.log(`juan` == `JUAN`);//false
console.log(`juan` ==  juan); //true

//(!=) Inequality Operator

//(===) strictly equality operator
console.log(`strictly`);
console.log(1===1); //true
console.log(1===2);  //true
console.log(1===`1`); //false
console.log(1=== false); //false
console.log('juan'==='JUAN'); //false
console.log('juan'===juan); //true

//relational comparison
//check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

//greater than (>)
console.log(x>y); //false
console.log();

//lees than or equal to
console.log(x <= y); //true
console.log(y <= y); //true

//logical operators
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical operators")
//logical and operator (&& - double ampersand)
//returns true if all operands are truec
let authorization1 = isAdmin && isRegistered;
console.log(authorization1) //false

let authorization2 = isAdmin && isRegistered;
console.log(authorization2) //true

let requiredLevel = 95;
let requiredAge = 18

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4); //true

let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge =  15;
let userAge2 = 30;
 
let registration1 = userName.length > 8 && userAge >=requiredAge;
console.log(registration1);

//or operator (|| - double pipe)
// returns true if atleast one of the operands are true

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1);


let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2); //false

//not operator
console.log("not operator");
//turns a boolean into the opposite value

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //true 

console.log(!isRegistered); //false

let opposite1 = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite1); //true
console.log(opposite2); //false

//if, else if, and else statement



//else if statement
if(userName2.length >=10 && userLevel2 <= 25 && userAge >= requiredAge){
	console.log("Thank you for joining the newbies guild")
} else if(userLevel > 25) {
	console.log("too strong");
} else if(userAge2 < requiredAge){
	console.log("too young");
}else {
	console.log("better luck next time");
}



//if, else if and else statement with function
function addNum(num1, num2){
	if(typeof num1 === "number" && num2 === "number"){
		console.log("run only if both arguments pass number types");
		console.log(num1 + num2);
	} else {
		console.log("one or both the arguments are not numbers");
	}

}


addNum(5, `2`);

//create log in function
/*
function login(username,password){
		if(username.length >= 8 && password.length >= 8){
			console.log("Thank you for logging in")
		}else{
			if(username.length >= 8){
			alert("Password too short")
			}else{
			alert("username too short")
			}
		}
}	

*/
//login("12345678", "jane123");

//function with return keyword

let message = `no message`;
console.log(message);

function determinTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return `Not a typhoon yet.`;
	}
	else if(windspeed <= 61){
		return `Tropical depression detected.`;
	}
	else if(windspeed >=62 && windspeed <= 88){
		return `Tropical storm detected`;
	}
	else if(windspeed >= 89 && windspeed <= 117){
		return `Severe tropical storm detected.`
	}
	else{
		return `Typhoon detected`;
	}
}

message = determinTyphoonIntensity(68);
console.log(message);
//console.warn()
if (message == `Tropical storm detected`){
	console.warn(message);
}

//truthy and falsy
//truty
if(true) {
	console.log(`truthy`)
}

if(1){
	console.log(`True`)
}

if([]){
	console.log(`truthy`)	
}


//falsy
// -0, "", null, Nan(not a number)
if(false){
	console.log(`Falsy`);
}

if(0){
	console.log(`Falsy`);	
}

if(undefined){
	console.log(`Falsy`)
}


//Ternary Operator

/*
Syntax;
	(expression/condition) ? iftrue : iffalse;
three operand of ternary operator:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is false
*/

let ternaryResult = (1 < 18) ? true : false;
console.log(`result of ternary operator ${ternaryResult}`);

let price = 50000;

price > 1000 ? console.log("price is over 1000") : console.log("price is less than 1000")

let villain = "Harvey Dent";

villain === "two face"
? console.log("you lived long enough to be a villain")
: console.log("not quite villainous yet.")




























































